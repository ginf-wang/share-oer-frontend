FROM node:14
COPY . .
RUN yarn install --silent
RUN yarn build

FROM nginx
COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=0 build/ /usr/share/nginx/html
