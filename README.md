# SHARE OER - Frontend (*english*)

*(german version below)*

This software was written as part of the Bachelor's Project BM4OER at the University of Bremen.
Being an early prototype, use in production is strongly discouraged!

We ended all official developments on 2020-06-08 hence we won't provide support or maintain this repo.

## Goal

This browser client is a proof-of-concept on how to implement the SHARE OER backend.

Available features are (limited to a demo account):
* Login, logout persisted via session storage.
* Search and filtering for OER.
* View of metadata and download of media items.
* Private conversations (read-only).

## How to build

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app) and hence built
with React. For installation or building instructions take a look at the excellent Create React App documentations.
 
It uses **yarn** but **npm** should work fine.

The Dockerfile in this repository builds the application and starts a simple _nginx_ instance.

## License

This project is licensed under GNU Affero General Public License v3. See [LICENSE](./LICENSE) for more information.

# SHARE OER - Frontend (_deutsch_)

Diese Software ist im Rahmen des Bachelorprojektes BM4OER der Universität Bremen entstanden. Sie stellt einen frühen Prototypen dar und kann keinesfalls produktiv verwendet werden!

Die Entwicklung wurde am 08.06.20 eingestellt, weshalb wir keinen Support anbieten und das Repository nicht weiter
gepflegt wird.

## Ziel

Dieser Browser-Client diente als Proof of Concept, wie das SHARE OER Backend beispielshaft implementiert werden kann.

Umgesetzte Funktionalitäten sind (limitiert auf einen Demonstrations-Account):
* Anmelden, Abmelden gesichert im Session-Storage des Browsers.
* Suche und Filterung von OER.
* Einsicht der Metadaten eines Mediums und Download des Items.
* Einsicht von privaten Chatverläufen (lediglich Lesezugriff)

## Anleitung

Das Projekt wurde mittels [Create React App](https://github.com/facebook/create-react-app) erstellt und verwendet
daher React. Für Installations- beziehungsweise Auslieferungshinweise kann die exzellente Create React App
Dokumentation konsultiert werden.
  
Zur Verwaltung wird bevorzugt **yarn** verwendet. **npm** sollte aber auch unterstützt sein.

Die Dockerfile dieses Repositories baut the Applikation und liefert sie mittels einer _nginx_-Instanz aus.

## Lizenz

Dieses Projekt ist lizensiert unter der GNU Affero General Public License v3. Siehe [LICENSE](./LICENSE) für mehr Informationen.

