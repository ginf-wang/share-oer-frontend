import React, { useCallback, useEffect, useMemo } from 'react';
import { useAsyncFn } from 'react-use';
import { useImmer } from 'use-immer';
import PropTypes from 'prop-types';
import { authContext } from '../../context/auth';
import axios from 'axios';

/**
 * Tracks the credentials of the current user. Whenever the credentials
 * change (login, logout) a default authorization header is set in axios.
 * This eliminates the need to always access the auth state to authenticate
 * a request.
 *
 * @author Ruben Smidt
 * @param credentials the credentials of the current user
 */
const useGlobalAxiosAuth = (credentials) => {
  useEffect(() => {
    if (credentials) {
      const { email, password } = credentials;
      axios.defaults.headers.common['Authorization'] = `Basic ${btoa(
        `${email}:${password}`
      )}`;
    } else {
      delete axios.defaults.headers.common['Authorization'];
    }
  }, [credentials]);
};

/**
 * Provides the authentication state for the application. This includes e.g.
 * login and logout capabilities.
 *
 * @author Ruben Smidt
 */
const AuthProvider = ({ children }) => {
  const [state, produceState] = useImmer(() => {
    let initialUser = null;
    const rawUser = sessionStorage.getItem('user');
    if (rawUser) {
      initialUser = JSON.parse(rawUser);
    }
    return {
      user: initialUser,
      processingAuth: false,
    };
  });

  const { user } = state;
  useGlobalAxiosAuth(user);

  const [loginState, loginUser] = useAsyncFn(async ({ email, password }) => {
    const response = await axios.get(
      `https://backend.bm4oer.de/user/${email}`,
      {
        auth: {
          username: email,
          password,
        },
      }
    );
    return {
      ...response.data,
      password,
    };
  }, []);

  useEffect(() => {
    if (!loginState.value) {
      return;
    }
    produceState((draft) => void (draft.user = loginState.value));
    sessionStorage.setItem('user', JSON.stringify(loginState.value));
  }, [produceState, loginState.value]);

  const logoutUser = useCallback(() => {
    sessionStorage.removeItem('user');
    produceState((draft) => void (draft.user = null));
  }, [produceState]);

  const value = useMemo(
    () => ({
      ...state,
      loginUser,
      logoutUser,
      processingState: loginState.loading,
      isAuthenticated: Boolean(state.user),
    }),
    [state, loginUser, logoutUser, loginState.loading]
  );

  return <authContext.Provider value={value}>{children}</authContext.Provider>;
};

AuthProvider.defaultProps = {
  children: PropTypes.node,
};

export default AuthProvider;
