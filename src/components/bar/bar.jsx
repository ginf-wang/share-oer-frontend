import React from 'react';
import PropTypes from 'prop-types';
import styles from './bar.module.scss';

/**
 * A bar that stretches vertically and has default styles.
 *
 * @author Dominc Maurer
 */
const Bar = ({ children }) => {
  return <div className={styles.background}>{children}</div>;
};

Bar.defaultProps = {
  children: PropTypes.node,
};

export default Bar;
