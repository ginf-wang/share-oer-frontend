import React from 'react';
import styles from './box.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import clsx from 'clsx';
import PropTypes from 'prop-types';

/**
 * A box with a default header and an outlet inside that box.
 * It embeds an icon with a click action.
 *
 * @author David Wagner
 */
const Box = ({
  children,
  headline,
  icon,
  onIconClick,
  outletClassName,
  disabled,
}) => {
  return (
    <div
      {...(disabled ? { 'data-tip': 'Funktion nicht unterstützt' } : {})}
      className={styles.box}
    >
      <div className={styles.head}>
        <span>{headline}</span>
        <FontAwesomeIcon
          className={styles.icon}
          onClick={onIconClick}
          icon={icon}
        />
      </div>
      <div className={clsx(outletClassName, styles.outlet)}>{children}</div>
    </div>
  );
};

Box.propTypes = {
  children: PropTypes.array,
  headline: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  disabled: PropTypes.bool,
};

export default Box;
