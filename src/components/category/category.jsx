import React from 'react';
import styles from './category.module.scss';
import PropTypes from 'prop-types';

/**
 * A styles list with an header.
 *
 * @author Dominic Maurer
 */
const Category = ({ headline, items }) => {
  return (
    <div className={styles.category}>
      <span>{headline}</span>
      {items.map((item, i) => (
        <div key={i} className={styles.item}>
          <span>{item}</span>
        </div>
      ))}
    </div>
  );
};

Category.propTypes = {
  headline: PropTypes.string.isRequired,
  items: PropTypes.array.isRequired,
};

export default Category;
