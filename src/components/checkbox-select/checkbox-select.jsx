import React from 'react';
import PropTypes from 'prop-types';
import styles from './checkbox-select.module.scss';

/**
 * A select component that displays its elements as checkboxes (rather than the
 * native browser style).
 *
 * @author Ruben Smidt
 */
const CheckboxSelect = ({ items, onChange, activeKeys = [] }) => {
  const handleSelection = ({ target: { value, checked } }) => {
    if (checked) {
      onChange([...activeKeys, value]);
    } else {
      onChange(activeKeys.filter((key) => key !== value));
    }
  };

  return (
    <div>
      {items.map(({ key, value }) => (
        <div key={key} className={styles.checkboxRow}>
          <label htmlFor={key}>{value}</label>
          <input
            onChange={handleSelection}
            id={key}
            type="checkbox"
            value={key}
            checked={activeKeys.includes(key)}
          />
        </div>
      ))}
    </div>
  );
};

CheckboxSelect.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      key: PropTypes.string.isRequired,
      value: PropTypes.any.isRequired,
    })
  ).isRequired,
  onChange: PropTypes.func.isRequired,
  activeKeys: PropTypes.arrayOf(PropTypes.string),
};

export default CheckboxSelect;
