import React from 'react';
import styles from './contacts-entry.module.scss';
import PropTypes from 'prop-types';

/**
 * A single entry of contacts usually composed of an avatar and the users name.
 *
 * @author David Wagner
 */
const ContactEntry = ({ user }) => (
  <div className={styles.item}>
    <img className={styles.pic} src={user.img} alt={'user avatar'} />
    <span className={styles.name}>{user.name}</span>
  </div>
);

ContactEntry.propTypes = {
  user: PropTypes.shape({
    img: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  }).isRequired,
};

export default ContactEntry;
