import React from 'react';
import Box from '../box';
import ContactEntry from './contacts-entry';
import styles from './contacts.module.scss';
import PropTypes from 'prop-types';

/**
 * Displays a list of contact entries inside a box.
 *
 * @author David Wagner
 */
const Contact = ({ users, onNewMessage }) => (
  <Box
    disabled
    children={users}
    headline="Kontakte"
    icon="user-plus"
    onIconClick={onNewMessage}
    outletClassName={styles.back}
  >
    {users.map((user) => (
      <ContactEntry key={user.id} user={user} />
    ))}
  </Box>
);

Contact.propTypes = {
  users: PropTypes.array.isRequired,
  onNewMessage: PropTypes.func.isRequired,
};

export default Contact;
