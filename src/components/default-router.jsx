import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import { isEmpty } from 'lodash';
import ReactTooltip from 'react-tooltip';
import { useAuth } from '../hooks/auth';
import Community from '../pages/community';
import FrontPage from '../pages';
import DefaultHeader from '../pages/default-header';
import Filter from '../pages/filter';

const AuthenticatedRoute = ({ children, ...rest }) => {
  const { user } = useAuth();
  return (
    <Route
      {...rest}
      render={() => (isEmpty(user) ? <Redirect to="/" /> : children)}
    />
  );
};

/**
 * The default router of the application.
 *
 * @author Ruben Smidt
 */
const DefaultRouter = () => (
  <>
    <DefaultHeader />
    <Switch>
      <Route exact path="/">
        <ReactTooltip effect="solid" />
        <FrontPage />
      </Route>
      <AuthenticatedRoute path="/community">
        <Community />
      </AuthenticatedRoute>
      <Route path="/filter">
        <Filter />
      </Route>
    </Switch>
  </>
);

export default DefaultRouter;
