import React from 'react';
import styles from './footer.module.scss';
import Bar from '../bar';
import Logo from './footer_logo.png';

/**
 * The default footer of the application.
 *
 * @autor Dominic Maurer
 */
const Footer = () => {
  return (
    <Bar>
      <img src={Logo} alt="Logo" />
      <div className={styles.start}>
        {}
        <button onClick={() => {}} className={styles.button}>
          Über uns
        </button>
        <button onClick={() => {}} className={styles.button}>
          Kontakt
        </button>
        <button onClick={() => {}} className={styles.button}>
          FAQ
        </button>
        <button onClick={() => {}} className={styles.button}>
          Rechtliches
        </button>
        <button onClick={() => {}} className={styles.button}>
          Impressum
        </button>
      </div>
    </Bar>
  );
};

export default Footer;
