import React from 'react';
import PropTypes from 'prop-types';
import Box from '../box';
import GroupsEntry from './groups-entry';
import styles from './groups-box.module.scss';

/**
 * Displays a list of groups a user belongs to inside a box.
 *
 * @author David Wagner
 */
const GroupsBox = ({
  groups,
  onAddMessage,
  onSettings,
  onMembers,
  onShare,
}) => (
  <Box
    disabled
    children={groups}
    headline="Gruppen"
    icon="plus-square"
    onIconClick={onAddMessage}
    outletClassName={styles.back}
  >
    {groups.map((group) => (
      <GroupsEntry
        key={group.id}
        group={group}
        onSettings={onSettings}
        onMembers={onMembers}
        onShare={onShare}
      />
    ))}
  </Box>
);

GroupsBox.defaultProps = {
  groups: PropTypes.array.isRequired,
  onAddMessage: PropTypes.func.isRequired,
  onSettings: PropTypes.func.isRequired,
  onMembers: PropTypes.func.isRequired,
  onShare: PropTypes.func.isRequired,
};

export default GroupsBox;
