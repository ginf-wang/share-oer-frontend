import React from 'react';
import PropTypes from 'prop-types';
import styles from './groups-entry.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

/**
 * A single group entry that renders various group related actions.
 *
 * @author David Wagner
 */
const GroupsEntry = ({ group, onSettings, onMembers, onShare }) => (
  <div className={styles.item}>
    <img className={styles.pic} src={group.img} alt={'user avatar'} />
    <p>
      <span className={styles.name}>{group.name}</span>
      <span className={styles.message}> {'\n' + group.lastMessage}</span>
    </p>
    <FontAwesomeIcon
      className={styles.icon}
      onClick={() => onShare(group)}
      icon="folder-plus"
    />

    <FontAwesomeIcon
      className={styles.icon2}
      onClick={() => onSettings(group)}
      icon="sliders-h"
    />

    <FontAwesomeIcon
      className={styles.icon2}
      onClick={() => onMembers(group)}
      icon="user-friends"
    />
  </div>
);

GroupsEntry.defaultProps = {
  group: PropTypes.shape({
    img: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    lastMessage: PropTypes.string.isRequired,
  }).isRequired,
  onSettings: PropTypes.func.isRequired,
  onMembers: PropTypes.func.isRequired,
  onShare: PropTypes.func.isRequired,
};

export default GroupsEntry;
