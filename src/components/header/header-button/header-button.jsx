import { createElement } from 'react';
import PropTypes from 'prop-types';
import styles from './header-button.module.scss';

/**
 * A default button used in the header.
 *
 * @author Ruben Smidt
 */
const HeaderButton = ({ host = 'button', ...rest }) => {
  return createElement(host, { className: styles.button, ...rest });
};

HeaderButton.propTypes = {
  // The type of the host component.
  host: PropTypes.elementType,
};

export default HeaderButton;
