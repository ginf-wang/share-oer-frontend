import React from 'react';
import PropTypes from 'prop-types';
import styles from './header.module.scss';
import Bar from '../bar';

/**
 * Renders its children inside a bar.
 *
 * @author Dominic Maurer
 * @author Ruben Smidt
 */
const Header = ({ children }) => {
  return (
    <Bar>
      <div className={styles.all}>{children}</div>
    </Bar>
  );
};

Header.defaultProps = {
  children: PropTypes.node,
};

export default Header;
