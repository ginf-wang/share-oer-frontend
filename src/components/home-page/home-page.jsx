import React, { useCallback } from 'react';
import Logo from './logo.png';
import styles from './home-page.module.scss';
import SearchField from './search-field';
import InfoField from './info-field';
import { useHistory } from 'react-router-dom';

/**
 * The home/landing page of the application.
 *
 * @author Felix Ziegler
 * @author Ruben Smidt
 */
const HomePage = () => {
  const history = useHistory();
  const goToSearch = useCallback(
    (query) => {
      history.push({
        pathname: '/filter',
        search: '?search=' + query,
      });
    },
    [history]
  );

  return (
    <div className={styles.homePage}>
      <img src={Logo} alt="Logo" />
      <SearchField onRequestSearch={goToSearch} />
      <div>
        <button
          data-tip="Funktion nicht unterstützt"
          disabled
          className={styles.buttonSearch}
          onClick={() => {}}
        >
          Erweiterte Suche
        </button>
        <button
          data-tip="Funktion nicht unterstützt"
          disabled
          className={styles.buttonProof}
          onClick={() => {}}
        >
          Prüfen
        </button>
      </div>
      <InfoField
        heading="Unsere Mission"
        text="Der Einsatz von Lehrmaterialien in Schulen beschränkt sich seit jeher fast ausschließlich auf Medien von kommerziellen Verlagen. Als mögliche Alternative existieren seit einiger Zeit offene Bildungsmedien, sogenannte Open Educational Ressources (kurz OER). Diese zeichnen sich vor allem durch die freie und wenig beschränkte Nutzung aus. Im Projekt BM4OER geht es darum, ob solch ein Modell auch in der realen Praxis eingesetzt und überleben kann."
      />
    </div>
  );
};

export default HomePage;
