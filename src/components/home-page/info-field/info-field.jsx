import React from 'react';
import PropTypes from 'prop-types';
import styles from './info-field.module.scss';

/**
 * An info field used on the home page.
 *
 * @author Felix Ziegler
 */
const InfoField = ({ heading, text }) => (
  <div className={styles.infoField}>
    <span className={styles.heading}>{heading}</span>
    <p>{text}</p>
  </div>
);

InfoField.defaultProps = {
  heading: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
};

export default InfoField;
