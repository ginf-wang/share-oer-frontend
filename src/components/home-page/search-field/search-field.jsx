import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useFormInput } from '../../../hooks/forms';
import styles from './search-field.module.scss';

/**
 * A large search input field used on the home page.
 *
 * @author Felix Ziegler
 * @author Ruben Smidt
 */
const SearchField = ({ onRequestSearch }) => {
  const searchInputProps = useFormInput({ onEnter: onRequestSearch });
  const requestSearch = useCallback(() => {
    onRequestSearch(searchInputProps.value);
  }, [onRequestSearch, searchInputProps.value]);

  return (
    <div className={styles.searchField}>
      <button className={styles.searchButton} onClick={requestSearch}>
        <FontAwesomeIcon
          className={styles.searchIcon}
          icon="search"
          size="2x"
        />
      </button>
      <input
        className={styles.rectangleSearch}
        {...searchInputProps}
        placeholder={'Suchbegriff eingeben...'}
      />
    </div>
  );
};

SearchField.defaultProps = {
  onRequestSearch: PropTypes.func.isRequired,
};

export default SearchField;
