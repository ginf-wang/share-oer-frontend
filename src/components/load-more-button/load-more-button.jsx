import React from 'react';
import styles from './load-more-button.module.scss';

/**
 * Button used to load more filter entries on the filter page.
 *
 * Proxy: input
 *
 * @author Ruben Smidt
 */
const LoadMoreButton = (props) => (
  <button className={styles.button} {...props}>
    Mehr laden
  </button>
);

export default LoadMoreButton;
