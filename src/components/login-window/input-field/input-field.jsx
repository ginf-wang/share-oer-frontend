import React from 'react';
import PropTypes from 'prop-types';
import styles from './input-field.module.scss';

/**
 * The default input field of the login window.
 *
 * Proxy: input
 *
 * @constructor Felix Ziegler
 */
const InputField = ({ children, ...rest }) => (
  <div className={styles.inputField}>
    <span className={styles.span}>{children}</span>
    <input type="text" className={styles.input} {...rest} />
  </div>
);

InputField.propTypes = {
  children: PropTypes.node.isRequired,
};

export default InputField;
