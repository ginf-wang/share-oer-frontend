import React from 'react';
import styles from './login-register.module.scss';

/**
 * The tab header that controls the login/register outlet.
 *
 * @author Felix Ziegler
 */
const LoginRegister = () => (
  <div className={styles.fieldRow}>
    <button className={styles.buttonLogin}>Anmelden</button>
    <button className={styles.buttonRegister}>Registrieren</button>
  </div>
);

export default LoginRegister;
