import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useFormInput } from '../../hooks/forms';
import styles from './login-window.module.scss';
import LoginRegister from './login-register';
import InputField from './input-field';

/**
 * Compact login window used on the home page.
 *
 * @author Felix Ziegler
 * @author Ruben Smidt
 */
const LoginWindow = ({ onSubmit }) => {
  const emailProps = useFormInput({ defaultValue: 'marv@bm4oer.de' });
  const passwordProps = useFormInput({ defaultValue: 'Schwertfisch' });

  const submit = useCallback(() => {
    onSubmit({ email: emailProps.value, password: passwordProps.value });
  }, [emailProps.value, onSubmit, passwordProps.value]);

  return (
    <div className={styles.loginWindow}>
      <LoginRegister />
      <FontAwesomeIcon icon="user" size="8x" color="#C4C4C4" />
      <span className={styles.login}>Anmelden</span>
      <div className={styles.form}>
        <InputField {...emailProps}>Email</InputField>
        <InputField type="password" {...passwordProps}>
          Passwort
        </InputField>
        <button>Passwort vergessen?</button>
      </div>
      <button className={styles.buttonLog} onClick={submit}>
        Anmelden
      </button>
    </div>
  );
};

LoginWindow.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

export default LoginWindow;
