import React from 'react';
import PropTypes from 'prop-types';
import Box from '../box';
import MessagesEntry from './messages-entry';
import styles from './messages-box.module.scss';

/**
 * Displays user conversation inside a box.
 *
 * @author David Wagner
 */
const MessagesBox = ({
  users,
  onAddMessage,
  onShareMessage,
  onSettingsMessage,
  onMessageClick,
}) => (
  <Box
    children={users}
    headline="Nachrichten"
    icon="comment-medical"
    onIconClick={onAddMessage}
    outletClassName={styles.back}
  >
    {users.map((user) => (
      <MessagesEntry
        key={user.id}
        user={user}
        onSettingsMessage={onSettingsMessage}
        onShareMessage={onShareMessage}
        onMessageClick={onMessageClick}
      />
    ))}
  </Box>
);

MessagesBox.propTypes = {
  users: PropTypes.array.isRequired,
  onAddMessage: PropTypes.func.isRequired,
  onShareMessage: PropTypes.func.isRequired,
  onSettingsMessage: PropTypes.func.isRequired,
  onMessageClick: PropTypes.func.isRequired,
};

export default MessagesBox;
