import React, { useCallback } from 'react';
import PropTypes from 'prop-types';
import styles from './messages-entry.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

/**
 * A message entry that displays the last message of a conversation.
 *
 * @author David Wagner
 */
const MessagesEntry = ({
  user,
  onShareMessage,
  onSettingsMessage,
  onMessageClick,
}) => {
  const handleEntryClicked = useCallback(() => {
    onMessageClick(user);
  }, [onMessageClick, user]);
  return (
    <div onClick={handleEntryClicked} className={styles.item}>
      <img className={styles.pic} src={user.img} alt={'user avatar'} />
      <div className={styles.texts}>
        <span className={styles.name}>{user.name}</span>
        <p className={styles.message}> {'\n' + user.lastMessage}</p>
      </div>
      <FontAwesomeIcon
        className={styles.icon}
        onClick={() => onShareMessage(user)}
        icon="folder-plus"
      />

      <FontAwesomeIcon
        className={styles.icon2}
        onClick={() => onSettingsMessage(user)}
        icon="sliders-h"
      />
    </div>
  );
};

MessagesEntry.propTypes = {
  user: PropTypes.shape({
    img: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    lastMessage: PropTypes.string.isRequired,
  }).isRequired,
  onShareMessage: PropTypes.func.isRequired,
  onSettingsMessage: PropTypes.func.isRequired,
  onMessageClick: PropTypes.func.isRequired,
};

export default MessagesEntry;
