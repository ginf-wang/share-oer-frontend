import React from 'react';
import PropTypes from 'prop-types';
import styles from './profile.module.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

/**
 * A overview of a users profile.
 *
 * @author David Wagner
 */
const Profile = ({ name, picture, memberSince }) => (
  <div>
    <div className={styles.pill}>
      <span className={styles.deleteFriend}>
        <FontAwesomeIcon icon="times" size="1x" onClick={() => {}} /> Freund
        entfernen
      </span>
    </div>

    <div className={styles.background}>
      <img className={styles.picture} src={picture} alt={'user avatar'} />
      <span className={styles.name}>{name}</span>
      <span className={styles.member}>Mitglied seit {memberSince}</span>

      <FontAwesomeIcon
        className={styles.button1}
        icon="user"
        size="2x"
        onClick={() => {}}
      />
      <span className={styles.buttonText} onClick={() => {}}>
        Details anzeigen
      </span>

      <FontAwesomeIcon
        className={styles.button}
        icon="envelope"
        size="2x"
        onClick={() => {}}
      />
      <span className={styles.buttonText} onClick={() => {}}>
        {' '}
        Privatnachricht senden
      </span>

      <FontAwesomeIcon
        className={styles.button}
        icon="user-plus"
        size="2x"
        onClick={() => {}}
      />
      <span className={styles.buttonText} onClick={() => {}}>
        In Gruppe einladen{' '}
      </span>
    </div>
  </div>
);

Profile.propTypes = {
  name: PropTypes.string.isRequired,
  picture: PropTypes.string.isRequired,
  memberSince: PropTypes.string.isRequired,
};

export default Profile;
