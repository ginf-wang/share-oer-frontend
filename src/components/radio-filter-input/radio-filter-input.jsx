import React from 'react';
import PropTypes from 'prop-types';
import styles from './radio-filter-input.module.scss';

/**
 * A single input of the radio filter. It's usually styled as a checkbox.
 *
 * @author Ruben Smidt
 */
const RadioFilterInput = ({ option, onClick, active }) => {
  return (
    <div className={styles.circle}>
      <span>{option}</span>
      <input checked={active} onClick={onClick} type="checkbox" />
    </div>
  );
};

RadioFilterInput.propTypes = {
  option: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  active: PropTypes.string.isRequired,
};

export default RadioFilterInput;
