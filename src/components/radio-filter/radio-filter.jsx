import React from 'react';
import PropTypes from 'prop-types';
import RadioFilterInput from '../radio-filter-input';

/**
 * A list of checkboxes that act as an radio input.
 *
 * @author Ruben Smidt
 */
const RadioFilter = ({ options, onOptionChange, activeOption }) => {
  const onChildrenClick = (option) => () => onOptionChange(option);

  return options.map((option) => (
    <RadioFilterInput
      key={option}
      active={option === activeOption}
      option={option}
      onClick={onChildrenClick(option)}
    />
  ));
};

RadioFilter.propTypes = {
  options: PropTypes.arrayOf(PropTypes.object).isRequired,
  onOptionChange: PropTypes.func.isRequired,
  activeOption: PropTypes.string.isRequired,
};

export default RadioFilter;
