import React from 'react';
import styles from './checkbox.module.scss';
import PropTypes from 'prop-types';

/**
 * A checkbox input used in the registration form.
 *
 * @author Dominic Maurer
 */
const Checkbox = ({ item, ...rest }) => (
  <div>
    <input {...rest} type="checkbox" className={styles.box} />
    <span className={styles.textStyle}>{item}</span>
  </div>
);

Checkbox.propTypes = {
  item: PropTypes.string.isRequired,
};

export default Checkbox;
