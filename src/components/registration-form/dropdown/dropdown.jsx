import React from 'react';
import styles from './dropdown.module.scss';
import PropTypes from 'prop-types';

/**
 * A simple dropdown with default values.
 *
 * @author Dominic Maurer
 */
const Dropdown = ({ item, id, ...rest }) => (
  <>
    <label htmlFor={id} className={styles.textStyle}>
      {item}
    </label>
    <select {...rest} id={id} className={styles.field}>
      <option selected value="default">
        Bitte auswählen
      </option>
      <option value="student">SchülerIn</option>
      <option value="teacher">LehrerIn</option>
      <option value="member">Mitglied</option>
    </select>
  </>
);

Dropdown.propTypes = {
  item: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
};

export default Dropdown;
