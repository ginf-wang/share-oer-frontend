import React from 'react';
import styles from './registration-form.module.scss';
import RegistrationInput from './registration-input';
import Dropdown from './dropdown';
import Checkbox from './checkbox';

/**
 * A form that allows registration of users.
 *
 * @author Dominic Maurer
 */
const RegistrationForm = () => (
  <div className={styles.background}>
    <button className={styles.button}>Zurück</button>
    <p className={styles.header}>
      Herzlich willkommen! Um an unserer Community teil zu haben, registriere
      dich bitte.
    </p>
    <div className={styles.inputs}>
      <RegistrationInput item={'Vor- und Nachname'} />
      <span className={styles.info}>
        Dein Name wird nicht für andere Nutzer auf dieser Plattform sichtbar
        sein.
      </span>
      <RegistrationInput item={'Username'} />
      <span className={styles.info}>
        Dein Username wird für jeden Nutzer auf dieser Plattform sichtbar sein.
      </span>
      <RegistrationInput item={'Passwort'} />
      <Dropdown item={'Status'} />
      <RegistrationInput item={'Schule'} />
    </div>
    <Checkbox
      item={
        'Hiermit stimme ich den Datenschutzbedingungen und den AGB-Bedingungen zu.'
      }
    />
    <Checkbox
      item={
        'Ich erkläre mich mit den Datenschutzbestimmungen nach der DSGVO einverstanden und achte auf einen respektvollen Umgang.'
      }
    />
    <button onClick={() => {}} className={styles.register}>
      Registrieren
    </button>
  </div>
);

export default RegistrationForm;
