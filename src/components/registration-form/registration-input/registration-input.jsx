import React from 'react';
import styles from './registration-input.module.scss';
import PropTypes from 'prop-types';

/**
 * A styles input component used in the registration form.
 *
 * @author Dominic Maurer
 */
const RegistrationInput = ({ item, id, ...rest }) => (
  <>
    <label htmlFor={id} className={styles.textStyle}>
      {item}
    </label>
    <input {...rest} id={id} className={styles.input} />
  </>
);

RegistrationInput.propTypes = {
  item: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
};

export default RegistrationInput;
