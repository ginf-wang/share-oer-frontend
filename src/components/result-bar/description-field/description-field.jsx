import React from 'react';
import styles from './description-field.module.scss';
import DescriptionIconBar from './description-icon-bar';
import PropTypes from 'prop-types';

const DescriptionField = ({ description, format, dType, subject }) => (
  <div className={styles.descriptionFlex}>
    <p className={styles.paragraph}>{description}</p>
    <DescriptionIconBar format={format} dType={dType} subject={subject} />
  </div>
);

DescriptionField.propTypes = {
  description: PropTypes.string,
  format: PropTypes.string,
  dType: PropTypes.string,
  subject: PropTypes.string,
};

export default DescriptionField;
