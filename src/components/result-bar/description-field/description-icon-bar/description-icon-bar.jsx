import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from './description-icon-bar.module.scss';
import PropTypes from 'prop-types';

const DescriptionIconBar = ({ format, dType, subject }) => (
  <div className={styles.descriptionIcon}>
    <FontAwesomeIcon icon="graduation-cap" />
    <span className={styles.descriptionParagraph}>{format}</span>
    <FontAwesomeIcon icon="file" />
    <span className={styles.descriptionParagraph}>{dType}</span>
    <FontAwesomeIcon icon="shapes" />
    <span className={styles.descriptionParagraph}>{subject}</span>
  </div>
);

DescriptionIconBar.propTypes = {
  format: PropTypes.string,
  dType: PropTypes.string,
  subject: PropTypes.string,
};

export default DescriptionIconBar;
