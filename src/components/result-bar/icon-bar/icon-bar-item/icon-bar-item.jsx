import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from './icon-bar-item.module.scss';
import PropTypes from 'prop-types';

/**
 * A single item in the group of stats.
 *
 * @author Felix Ziegler
 */
const IconBarItem = ({ item, icon }) => (
  <div className={styles.iconText}>
    <FontAwesomeIcon icon={icon} />
    <span className={styles.iconItem}>{item}</span>
  </div>
);

IconBarItem.propTypes = {
  item: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
};

export default IconBarItem;
