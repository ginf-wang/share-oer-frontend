import React from 'react';
import styles from './icon-bar.module.scss';
import IconBarItem from './icon-bar-item';
import PropTypes from 'prop-types';

/**
 * A group of stats that are displayed with an icon.
 *
 * @author Felix Ziegler
 */
const IconBar = ({ author, date, evaluation }) => (
  <div className={styles.iconFlex}>
    <IconBarItem item={author} icon={'user'} />
    <IconBarItem item={date} icon={'calendar'} />
    <IconBarItem item={evaluation} icon={'star'} />
  </div>
);

IconBar.propTypes = {
  author: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  evaluation: PropTypes.string.isRequired,
};

export default IconBar;
