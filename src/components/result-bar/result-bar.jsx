import React from 'react';
import styles from './result-bar.module.scss';
import IconBar from './icon-bar';
import DescriptionField from './description-field';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PropTypes from 'prop-types';

/**
 * The entry of a filters search result. It shows compacted information
 * about the media item but can be expanded to show more detailed information.
 *
 * @author Felix Ziegler
 */
const ResultBar = ({
  description,
  format,
  dType,
  subject,
  author,
  date,
  evaluation,
  onClick,
}) => (
  <div onClick={onClick} className={styles.flexContainer}>
    <FontAwesomeIcon icon="clipboard" size="5x" />
    <DescriptionField
      description={description}
      format={format}
      dType={dType}
      subject={subject}
    />
    <IconBar author={author} date={date} evaluation={evaluation} />
  </div>
);

ResultBar.propTypes = {
  description: PropTypes.string.isRequired,
  format: PropTypes.string.isRequired,
  dType: PropTypes.string.isRequired,
  subject: PropTypes.string.isRequired,
  author: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  evaluation: PropTypes.string.isRequired,
  onClick: PropTypes.func,
};

export default ResultBar;
