import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PropTypes from 'prop-types';
import React from 'react';
import styles from './result-detail-info.module.scss';

/**
 * Lists all the stats of the media item filter search result.
 *
 * @author David Wagner
 * @author Ruben Smidt
 */
const ResultDetailInfo = ({
  description,
  uploadDate,
  changeDate,
  author,
  subject,
  gradeLevel,
  media,
  dataFormat,
  size,
  evaluation,
  downloads,
}) => (
  <div className={styles.back}>
    <FontAwesomeIcon className={styles.lone} icon="clipboard" size="5x" />
    <h1>{description}</h1>

    <span className={styles.mthree}>Hochgeladen am: {uploadDate}</span>
    <span className={styles.mfour}>Letzte Änderung: {changeDate}</span>
    <span className={styles.mfive}>Autor: {author}</span>
    <span className={styles.msix}>Schulfach: {subject}</span>
    <span className={styles.mseven}>Klassenstufe: {gradeLevel}</span>
    <span className={styles.meight}>Medienform: {media}</span>
    <span className={styles.mnine}>Dateiformat: {dataFormat}</span>
    <span className={styles.mten}>Größe: {size}</span>

    <span className={styles.rone}>Bewertung: {evaluation}</span>
    <span className={styles.rtwo}>Downloads: {downloads} </span>
  </div>
);

ResultDetailInfo.propTypes = {
  description: PropTypes.string.isRequired,
  uploadDate: PropTypes.string.isRequired,
  changeDate: PropTypes.string.isRequired,
  author: PropTypes.string.isRequired,
  subject: PropTypes.string.isRequired,
  gradeLevel: PropTypes.string.isRequired,
  media: PropTypes.string.isRequired,
  dataFormat: PropTypes.string.isRequired,
  size: PropTypes.string,
  evaluation: PropTypes.string.isRequired,
  downloads: PropTypes.number.isRequired,
};

export default ResultDetailInfo;
