import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import React from 'react';
import TagPill from '../tag-bar/tag-pill';
import ResultDetailInfo from './result-detail-info';
import styles from './result-detail.module.scss';

const IconButton = ({
  icon,
  host: Host = 'button',
  iconProps = {},
  ...rest
}) => {
  const { disabled = false } = rest;

  return (
    <div className={clsx(styles.iconButton, { [styles.disabled]: disabled })}>
      <FontAwesomeIcon {...iconProps} icon={icon} />
      <Host {...rest} className={styles.host} />
    </div>
  );
};

/**
 * The expanded and detailed view of a media item filter search result
 *
 * @author David Wagner
 * @author Ruben Smidt
 */
const ResultDetail = ({
  description,
  uploadDate,
  changeDate,
  author,
  subject,
  gradeLevel,
  media,
  dataFormat,
  size,
  evaluation,
  downloads,
  comments,
  href,
  onClick,
  tags = [],
}) => (
  <div onClick={onClick} className={styles.back}>
    <ResultDetailInfo
      description={description}
      uploadDate={uploadDate}
      changeDate={changeDate}
      author={author}
      subject={subject}
      gradeLevel={gradeLevel}
      media={media}
      dataFormat={dataFormat}
      size={size}
      evaluation={evaluation}
      downloads={downloads}
      comments={comments}
    />

    <div className={styles.tags}>
      <span>Schlagworte</span>
      {tags.map((tag) => (
        <TagPill key={tag} tag={tag} />
      ))}
    </div>

    <div className={styles.bottom}>
      <IconButton icon="download" host="a" target="_blank" href={href}>
        Herunterladen
      </IconButton>
      <IconButton disabled icon="download">
        Teilen
      </IconButton>
      <IconButton disabled icon="download">
        Inhalt melden
      </IconButton>
      <IconButton disabled icon="download">
        Zu meinen Dateien hinzufügen
      </IconButton>
    </div>
  </div>
);

ResultDetail.propTypes = {
  description: PropTypes.string.isRequired,
  uploadDate: PropTypes.string.isRequired,
  changeDate: PropTypes.string.isRequired,
  author: PropTypes.string.isRequired,
  subject: PropTypes.string.isRequired,
  gradeLevel: PropTypes.string.isRequired,
  media: PropTypes.string.isRequired,
  dataFormat: PropTypes.string.isRequired,
  size: PropTypes.string,
  evaluation: PropTypes.string.isRequired,
  downloads: PropTypes.number.isRequired,
  comments: PropTypes.array,
  href: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  tags: PropTypes.arrayOf(PropTypes.string),
};

export default ResultDetail;
