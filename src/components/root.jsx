import React, { Suspense } from 'react';
import { BrowserRouter } from 'react-router-dom';
import { RecoilRoot } from 'recoil';
import AuthProvider from './auth-provider';
import DefaultRouter from './default-router';

/**
 * The root of the application that acts as an entry to the application.
 * It wraps the pages of the application with providers.
 *
 * @author Ruben Smidt
 */
const Root = () => (
  <RecoilRoot>
    <Suspense fallback={null}>
      <BrowserRouter>
        <AuthProvider>
          <DefaultRouter />
        </AuthProvider>
      </BrowserRouter>
    </Suspense>
  </RecoilRoot>
);

export default Root;
