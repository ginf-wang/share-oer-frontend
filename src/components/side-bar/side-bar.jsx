import React, { memo, useCallback } from 'react';
import { useAsync } from 'react-use';
import { useRecoilState, useSetRecoilState } from 'recoil';
import axios from 'axios';
import { last } from 'lodash';
import { useHistory } from 'react-router-dom';
import { useAuth } from '../../hooks/auth';
import {
  selectReceiverFromDialog,
  userDialogsState,
  userMessagesState,
} from '../../state/community';
import GroupsBox from '../groups-box';
import Contact from '../contacts';
import MessagesBox from '../messages-box';

const GROUP_FIXTURES = [
  {
    id: 1,
    img: 'https://via.placeholder.com/300',
    name: 'Testgruppe',
    lastMessage: 'Nachricht aus der Gruppe!',
  },
  {
    id: 2,
    img: 'https://via.placeholder.com/300',
    name: 'Testgruppe 2',
    lastMessage: 'Nachricht aus der Gruppe 2!',
  },
];

const CONTACT_FIXTURES = [
  {
    id: 1,
    img: 'https://via.placeholder.com/300',
    name: 'Dank Memerino',
  },
  {
    id: 2,
    img: 'https://via.placeholder.com/300',
    name: 'Tester Testerino',
  },
];

/**
 * The sidebar container that issues requests to fetch messages, contact
 * infos etc.
 *
 * @author David Wagner
 * @author Ruben Smidt
 */
const SideBar = () => {
  const [messages, setMessages] = useRecoilState(userMessagesState);
  const setDialogs = useSetRecoilState(userDialogsState);
  const { user } = useAuth();
  const history = useHistory();

  useAsync(async () => {
    const response = await axios
      .get('https://backend.bm4oer.de/message', {
        auth: {
          password: user.password,
          username: user.email,
        },
      })
      .catch(() => history.push('/'));

    const msg = response.data.map((dialog) => {
      const lastMsg = last(dialog);
      const text =
        lastMsg.sender.username === user.username
          ? `Ich: ${lastMsg.message}`
          : lastMsg.message;

      return {
        id: lastMsg.id,
        name: selectReceiverFromDialog(dialog),
        lastMessage: text,
        img: 'https://via.placeholder.com/300',
      };
    });

    setMessages(msg);
    setDialogs(response.data);
  }, [setMessages, setDialogs, history, user]);

  const navigateToMessage = useCallback(
    (user) => {
      history.push(`/community/messages/${btoa(user.name)}`);
    },
    [history]
  );

  return (
    <div>
      <GroupsBox
        groups={GROUP_FIXTURES}
        onShare={() => {}}
        onMembers={() => {}}
        onSettings={() => {}}
        onAddMessage={() => {}}
      />
      <Contact users={CONTACT_FIXTURES} onNewMessage={() => {}} />
      <MessagesBox
        users={messages}
        onAddMessage={() => {}}
        onSettingsMessage={() => {}}
        onShareMessage={() => {}}
        onMessageClick={navigateToMessage}
      />
    </div>
  );
};

export default memo(SideBar);
