import React from 'react';
import TagPill from './tag-pill';
import styles from './tab-bar.module.scss';
import PropTypes from 'prop-types';

/**
 * A bar that lists all tags of a media item.
 *
 * @author Felix Ziegler
 */
const TagBar = ({ tags }) =>
  tags.map((tag) => (
    <div key={tag} className={styles.rectangleBlue}>
      <TagPill tag={tag} />
    </div>
  ));

TagBar.propTypes = {
  tags: PropTypes.array.isRequired,
};

export default TagBar;
