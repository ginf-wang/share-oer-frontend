import React from 'react';
import styles from './tag-pill.module.scss';
import PropTypes from 'prop-types';

/**
 * A single styled tag.
 *
 * @author Felix Ziegler
 */
const TagPill = ({ tag }) => (
  <div className={styles.oval}>
    <span className={styles.textStyle}>{tag}</span>
  </div>
);

TagPill.propTypes = {
  tag: PropTypes.string.isRequired,
};

export default TagPill;
