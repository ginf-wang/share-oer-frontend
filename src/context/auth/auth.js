import { createContext } from 'react';

// The default auth context that is used throughout the application.
export const authContext = createContext({});
