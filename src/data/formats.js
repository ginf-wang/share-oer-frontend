export const FORMATS = ['pdf', 'mp4', 'png', 'odt', 'txt', 'jpg'].map(
  (format) => ({
    key: format,
    value: format,
  })
);
