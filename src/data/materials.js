import { capitalize } from 'lodash';

export const MATERIAL_TYPES = [
  'ZUSAMMENFASSUNG',
  'ARBEITSBLATT',
  'VORLESUNG',
  'VIDEO',
  'KLAUSUR',
  'SPRACHAUFZEICHNUNG',
  'BILD',
  'SONSTIGES',
].map((materialType) => ({
  key: materialType,
  value: capitalize(materialType),
}));
