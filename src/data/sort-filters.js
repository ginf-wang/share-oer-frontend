export const SORT_TYPES = [
  {
    value: 'NAME_DESC',
    label: 'Name absteigend',
  },
  {
    value: 'NAME_ASC',
    label: 'Name aufsteigend',
  },
  {
    value: 'RATING_ASC',
    label: 'Bewertung aufsteigend',
  },
  {
    value: 'RATING_DESC',
    label: 'Bewertung absteigend',
  },
  {
    value: 'DOWNLOADS_ASC',
    label: 'Downloadzahl aufsteigend',
  },
  {
    value: 'DOWNLOADS_DESC',
    label: 'Downloadzahl absteigend',
  },
  {
    value: 'DATE_ASC',
    label: 'Älteste -> Neuste',
  },
  {
    value: 'DATE_DESC',
    label: 'Neuste -> Älteste',
  },
];
