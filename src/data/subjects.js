import { capitalize } from 'lodash';

export const SUBJECT_TYPES = [
  'FACH WÄHLEN',
  'BIOLOGIE',
  'CHEMIE',
  'DEUTSCH',
  'ENGLISCH',
  'MATHE',
  'MUSIK',
  'FRANZOESISCH',
  'SPANISCH',
  'INFORMATIK',
  'BWL',
  'KUNST',
  'GESCHICHTE',
  'RELIGION',
  'ETHIK',
  'SPORT',
  'PHYSIK',
  'ERDKUNDE',
  'SONSTIGES',
].map((subject) => ({
  value: subject,
  label: capitalize(subject),
}));
