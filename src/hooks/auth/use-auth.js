import { useContext } from 'react';
import { authContext } from '../../context/auth';

/**
 * A thin wrapper around the auth context that simplifies access to the auth
 * state.
 *
 * @author Ruben Smidt
 * @returns {object} the auth state.
 */
export const useAuth = () => {
  return useContext(authContext);
};
