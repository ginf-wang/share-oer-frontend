import { useMemo, useState } from 'react';

/**
 * Keeps track of the state of a select-like input composed of checkboxes.
 *
 * @author Ruben Smidt
 * @param items the individual options that get transformed into checkboxes
 * @param initialActive the options that are active to begin with
 * @returns {object} the properties of the input
 */
export const useCheckboxSelect = ({ items, initialActive = [] }) => {
  const [activeKeys, setActiveKeys] = useState(initialActive);
  return useMemo(
    () => ({
      onChange: setActiveKeys,
      activeKeys,
      items,
    }),
    [activeKeys, items]
  );
};
