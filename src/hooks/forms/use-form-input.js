import { useCallback, useRef, useState } from 'react';

/**
 * Constructs a callback with a stable reference. This callback will always
 * invoke the latest callback passed into this hook.
 *
 * A static reference prevents unnecessary rerenders. It should only be used
 * if you don't rely on reference of the callback to change (e.g. in
 * effect-hooks).
 *
 * @author Ruben Smidt
 * @param cb the callback to stabilize
 * @returns {function} a function with stable reference
 */
const useStableCallback = (cb) => {
  const cbRef = useRef(cb);
  cbRef.current = cb;

  return useCallback((...args) => {
    return cbRef.current(...args);
  }, []);
};

export const useFormInput = ({ onEnter, defaultValue = '', ...rest } = {}) => {
  const [value, setValue] = useState(defaultValue);
  const onChange = useCallback((event) => {
    event.persist();
    setValue(event.target.value);
  }, []);
  const onCommit = useStableCallback(() => onEnter?.(value));
  const onKeyDown = useStableCallback((event) => {
    if (event.key === 'Enter') {
      onCommit();
    }
  });

  return {
    ...rest,
    value,
    onChange,
    onKeyDown,
    onCommit,
  };
};
