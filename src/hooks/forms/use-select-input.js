import { useCallback, useState } from 'react';

/**
 * Manages the state of a select field.
 *
 * @author Ruben Smidt
 * @param options the options of the select field
 * @param defaultValue the default value
 * @returns {object} the properties of the select field
 */
export const useSelectInput = ({ options, defaultValue } = {}) => {
  const [value, setValue] = useState(defaultValue);
  const onChange = useCallback((newOption) => {
    setValue(newOption);
  }, []);
  return {
    value,
    options,
    onChange,
  };
};
