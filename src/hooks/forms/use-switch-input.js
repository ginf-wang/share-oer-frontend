import { useCallback } from 'react';
import { useToggle } from 'react-use';

/**
 * Manages inputs that can only have two states (on, off).
 *
 * @author Ruben Smidt
 * @param onCommit gets called when an action happens (state changes)
 * @param defaultValue the initial state
 * @returns {object} the properties of the input
 */
export const useSwitchInput = ({ onCommit, defaultValue = false } = {}) => {
  const [checked, toggle] = useToggle(defaultValue);
  const onChange = useCallback(() => {
    toggle();
    onCommit && onCommit(checked);
  }, [checked, onCommit, toggle]);
  return {
    checked,
    onChange,
  };
};
