import { useLocation } from 'react-router-dom';

/**
 * Reads the query parameters of the current locations.
 *
 * @author Ruben Smidt
 * @returns {URLSearchParams} a map of query parameters.
 */
export const useQuery = () => new URLSearchParams(useLocation().search);

/**
 * Gets the value of a specific query parameter.
 *
 * @author Ruben Smidt
 * @param {string } key the name of the query parameter
 * @returns {void|string} the value of the query parameter if present
 */
export const useQueryParam = (key) => useQuery().get(key);
