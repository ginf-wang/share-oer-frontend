import './font-awesome';

import React from 'react';
import ReactDOM from 'react-dom';
import Root from './components';
import './baseline.scss';

ReactDOM.render(<Root />, document.getElementById('root'));
