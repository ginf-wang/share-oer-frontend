import React, { memo, useMemo } from 'react';
import { isNil, groupBy } from 'lodash';
import { useParams } from 'react-router-dom';
import ReactTooltip from 'react-tooltip';
import { useRecoilValue } from 'recoil';
import { userDialogsState } from '../../../state/community';
import ChatEntry from '../chat-entry';
import styles from './chat-conversations.module.scss';

/**
 * Displays the conversation with a single user. Has capabilities to send
 * new messages.
 *
 * @author Ruben Smidt
 */
const ChatConversation = () => {
  const dialogs = useRecoilValue(userDialogsState);
  const { receiverId } = useParams();
  const username = useMemo(() => atob(receiverId), [receiverId]);
  const currentDialog = useMemo(
    () =>
      dialogs?.find((dialog) =>
        dialog.some(
          ({ sender, receiver }) =>
            sender.username === username || receiver.username === username
        )
      ),
    [dialogs, username]
  );
  if (isNil(currentDialog)) {
    return null;
  }

  const dialogDateChunks = groupBy(currentDialog, ({ dateSent }) => {
    const parsedDate = new Date(dateSent);
    const day = parsedDate.getDate();
    const month = parsedDate.getMonth() + 1;
    const year = parsedDate.getFullYear();

    return `${year}-${month}-${day}`;
  });

  const entries = Object.keys(dialogDateChunks).flatMap((date) => {
    return [
      <span key={date} className={styles.dateSeparator}>
        {new Date(date).toLocaleDateString()}
      </span>,
      dialogDateChunks[date].map((message) => {
        return <ChatEntry key={message.id} {...message} />;
      }),
    ];
  });

  return (
    <div className={styles.wrapper}>
      <ReactTooltip effect="solid" />
      <h1>Dein Verlauf mit: {username}</h1>
      <div className={styles.messageBox}>{entries}</div>
      <div className={styles.messageControl}>
        <input
          placeholder="Nachricht..."
          className={styles.input}
          type="text"
        />
        <button data-tip="Funktion nicht unterstützt" disabled>
          Senden
        </button>
      </div>
    </div>
  );
};

export default memo(ChatConversation);
