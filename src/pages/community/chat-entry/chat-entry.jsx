import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import React, { memo, useMemo } from 'react';
import styles from '../chat-entry.module.scss';

/**
 * One entry of a chat conversation. It handles whether the sender is the
 * current user or not.
 *
 * @author Ruben Smidt
 */
const ChatEntry = ({ sender, message, dateSent }) => {
  const avatar = sender.picture ? (
    <img src={sender.picture} alt="user avatar" />
  ) : (
    <div className={styles.iconWrapper}>
      <FontAwesomeIcon size="lg" icon="user" />
    </div>
  );
  const reversed = sender.username === 'Mervi';
  const username = reversed ? `Du (${sender.username})` : sender.username;
  const date = useMemo(() => new Date(dateSent), [dateSent]);

  return (
    <div className={clsx(styles.entry, { [styles.reversed]: reversed })}>
      <div className={styles.avatar}>{avatar}</div>
      <div className={styles.name}>{`${username}:`}</div>
      <div className={styles.message}>{message}</div>
      <div className={styles.date}>{date.toLocaleTimeString()}</div>
    </div>
  );
};

ChatEntry.propTypes = {
  sender: PropTypes.shape({
    username: PropTypes.string.isRequired,
    picture: PropTypes.string,
  }).isRequired,
  message: PropTypes.string.isRequired,
  dateSent: PropTypes.string.isRequired,
};

export default memo(ChatEntry);
