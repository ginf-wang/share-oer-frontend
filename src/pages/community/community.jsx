import React from 'react';
import { Route, Switch, useRouteMatch } from 'react-router-dom';
import ReactTooltip from 'react-tooltip';
import SideBar from '../../components/side-bar';
import { useAuth } from '../../hooks/auth';
import ChatConversation from './chat-conversation';
import styles from './community.module.scss';

/**
 * The community page displays social elements the user is associated with e.g.
 * messages, groups and contacts.
 *
 * @author Ruben Smidt
 */
const Community = () => {
  const { user } = useAuth();
  const match = useRouteMatch();

  return (
    <div className={styles.wrapper}>
      <SideBar />
      <div className={styles.outlet}>
        <Switch>
          <Route exact path={`${match.path}`}>
            <ReactTooltip effect="solid" />
            {user && <p>Willkommen in der Community {user.username}</p>}
          </Route>
          <Route path={`${match.path}/messages/:receiverId`}>
            <ChatConversation />
          </Route>
        </Switch>
      </div>
    </div>
  );
};

export default Community;
