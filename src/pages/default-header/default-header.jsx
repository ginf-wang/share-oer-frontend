import React from 'react';
import { Link } from 'react-router-dom';
import Header from '../../components/header';
import HeaderButton from '../../components/header/header-button';
import styles from '../../components/header/header.module.scss';
import { useAuth } from '../../hooks/auth';

/**
 * The default implementation of an header. It's used throughout the application
 * and acts on the authentication status.
 *
 * @author Ruben Smidt
 */
const DefaultHeader = () => {
  const { isAuthenticated, logoutUser } = useAuth();
  return (
    <>
      <Header>
        <div className={styles.grid}>
          <div className={styles.left}>
            <HeaderButton host={Link} to="/">
              Startseite
            </HeaderButton>
            <HeaderButton
              data-tip="Funktion nicht unterstützt"
              disabled
              onClick={() => {}}
            >
              Hochladen
            </HeaderButton>
            <HeaderButton host={Link} to="/filter">
              Suchen
            </HeaderButton>
          </div>
          <h1 className={styles.header}>SHARE OER</h1>
          <div className={styles.right}>
            {isAuthenticated && (
              <>
                <HeaderButton host={Link} to="/community">
                  Mein Bereich
                </HeaderButton>
                <HeaderButton data-tip="Funktion nicht unterstützt" disabled>
                  Profil
                </HeaderButton>
                <HeaderButton onClick={logoutUser}>Abmelden</HeaderButton>
              </>
            )}
          </div>
        </div>
      </Header>
    </>
  );
};

export default DefaultHeader;
