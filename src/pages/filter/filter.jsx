import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import { useAsync, useAsyncFn } from 'react-use';
import { atom, useRecoilState } from 'recoil';
import axios from 'axios';
import { isEmpty, last } from 'lodash';
import Switch from 'react-switch';
import Select from 'react-select';
import SliderBase from 'rc-slider';
import 'rc-slider/assets/index.css';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import CheckboxSelect from '../../components/checkbox-select';
import LoadMoreButton from '../../components/load-more-button';
import { FORMATS } from '../../data/formats';
import { MATERIAL_TYPES } from '../../data/materials';
import { SORT_TYPES } from '../../data/sort-filters';
import { SUBJECT_TYPES } from '../../data/subjects';
import {
  useCheckboxSelect,
  useFormInput,
  useSelectInput,
  useSwitchInput,
} from '../../hooks/forms';
import { useQueryParam } from '../../hooks/history';
import SearchInput from './search-input';
import ResultAdapter from './result-adapter';
import styles from './filter.module.scss';

const Range = SliderBase.createSliderWithTooltip(SliderBase.Range);

// Contains all media items that are currently displayed.
const mediaItemState = atom({
  key: 'BM4OER/filter/media-items',
  default: [],
});

// Tracks the latest active filter.
const latestFilterState = atom({
  key: 'BM4OER/filter/latest-filter',
  default: null,
});

const DEFAULT_RESULT_SIZE = 10;

const RANGE_MARKS = {
  1: { label: '1', style: { color: 'black' } },
  13: { label: '13', style: { color: 'black' } },
};
const RANGE_DEFAULT = [1, 13];

/**
 * Turn a date into a list that the backend understands.
 *
 * @param {Date} date the date to transform
 * @returns {array<number>} a list of date parts
 */
const formatDateToArray = (date) => [
  date.getFullYear(),
  date.getMonth() + 1,
  date.getDate(),
];

/**
 * The filter page offers a rich interface to query for media.
 *
 * @author Ruben Smidt
 */
const Filter = () => {
  const initialFilter = useQueryParam('search');

  const [hasMoreItems, setHasMoreItems] = useState(true);
  const [classRange, setClassRange] = useState([]);
  const [startDate, setStartDate] = useState(new Date(2000, 0, 1));
  const [endDate, setEndDate] = useState(new Date());
  const [mediaItems, setMediaItems] = useRecoilState(mediaItemState);
  const [latestFilter, setLatestFilter] = useRecoilState(latestFilterState);
  const lastMediaItem = useMemo(() => {
    return last(mediaItems) ?? undefined;
  }, [mediaItems]);
  const materialTypeCheckboxProps = useCheckboxSelect({
    items: MATERIAL_TYPES,
  });
  const formatCheckboxProps = useCheckboxSelect({ items: FORMATS });
  const subjectSelectProps = useSelectInput({
    defaultValue: SUBJECT_TYPES[0],
    options: SUBJECT_TYPES,
  });
  const sortBySelectProps = useSelectInput({
    defaultValue: SORT_TYPES[0],
    options: SORT_TYPES,
  });
  const toggleTagsProps = useSwitchInput({ defaultValue: false });

  const [, persistSearch] = useAsyncFn(
    async (searchTerm, filters, sort) => {
      const response = await axios.post('https://backend.bm4oer.de/search', {
        searchTerm,
        filters,
        sort,
      });
      setLatestFilter(response.data);
      return response.data;
    },
    [setLatestFilter]
  );

  // If one of the filter controls have changed we'll construct a new set of
  // request-ready filter objects.
  const filters = useMemo(() => {
    const filters = [];
    if (!isEmpty(materialTypeCheckboxProps.activeKeys)) {
      filters.push({
        type: 'materialtype',
        materialType: materialTypeCheckboxProps.activeKeys,
      });
    }
    if (!isEmpty(formatCheckboxProps.activeKeys)) {
      filters.push({
        type: 'format',
        fileFormat: formatCheckboxProps.activeKeys,
      });
    }
    if (!isEmpty(classRange)) {
      const [fromClass, toClass] = classRange;
      if (fromClass !== 1 || toClass !== 13) {
        filters.push({
          type: 'class',
          fromClass,
          toClass,
        });
      }
    }
    if (subjectSelectProps.value.label !== 'Fach wählen') {
      filters.push({
        type: 'subject',
        subjects: subjectSelectProps.value.value,
      });
    }
    filters.push({
      type: 'date',
      fromDate: formatDateToArray(startDate),
      toDate: formatDateToArray(endDate),
    });
    return filters;
  }, [
    materialTypeCheckboxProps.activeKeys,
    formatCheckboxProps.activeKeys,
    classRange,
    subjectSelectProps.value.label,
    subjectSelectProps.value.value,
    startDate,
    endDate,
  ]);

  const searchInputProps = useFormInput({
    defaultValue: initialFilter ?? '',
    onEnter: (value) => persistSearch(value, filters),
    placeholder: 'Suchbegriff',
  });

  const commitSearch = useCallback(() => {
    persistSearch(
      searchInputProps.value,
      filters,
      sortBySelectProps.value.label === 'Sortierung auswählen'
        ? null
        : sortBySelectProps.value.value
    );
  }, [searchInputProps.value, filters, persistSearch, sortBySelectProps.value]);

  const prevSortRef = useRef(sortBySelectProps.value);

  // Commits a new search if the sort-type changes.
  useEffect(() => {
    const sortValue = sortBySelectProps.value;
    const isSortDiff = prevSortRef.current !== sortValue;
    if (isSortDiff) {
      prevSortRef.current = sortValue;
      if (sortValue !== 'Sortierung auswählen') {
        commitSearch();
      }
    }
  }, [sortBySelectProps.value, commitSearch]);

  // Trigger an automatic fetch if the filter changes.
  useAsync(async () => {
    if (!latestFilter) {
      return;
    }
    const response = await axios.get('https://backend.bm4oer.de/media', {
      params: {
        searchId: latestFilter.id,
      },
    });
    const newItems = response.data;
    setMediaItems(newItems);
    setHasMoreItems(newItems.length === DEFAULT_RESULT_SIZE);
  }, [latestFilter]);

  // Request more items via the id of the latest media item.
  // If there aren't more items it'll disable the load-more button.
  const requestMoreItems = useCallback(async () => {
    if (!latestFilter) {
      return;
    }
    const response = await axios.get('https://backend.bm4oer.de/media', {
      params: {
        searchId: latestFilter.id,
        id: lastMediaItem.id,
      },
    });
    const newItems = response.data;
    if (isEmpty(newItems) || newItems.length < DEFAULT_RESULT_SIZE + 1) {
      setHasMoreItems(false);
    } else {
      setMediaItems((items) => [...items, ...response.data]);
    }
  }, [lastMediaItem, latestFilter, setMediaItems]);

  return (
    <div className={styles.wrapper}>
      <div className={styles.sidebar}>
        <div className={styles.sidebarWrapper}>
          <div className={styles.section}>
            <h1>Klassenstufen</h1>
            <Range
              marks={RANGE_MARKS}
              defaultValue={RANGE_DEFAULT}
              min={1}
              max={13}
              onChange={setClassRange}
            />
          </div>
          <div className={styles.section}>
            <h1>Fach</h1>
            <Select {...subjectSelectProps} />
          </div>
          <div className={styles.section}>
            <h1>Materialart</h1>
            <CheckboxSelect {...materialTypeCheckboxProps} />
          </div>
          <div className={styles.section}>
            <h1>Format</h1>
            <CheckboxSelect {...formatCheckboxProps} />
          </div>
          <div className={styles.section}>
            <h1>Erscheinungsdatum</h1>
            <div className={styles.dateRange}>
              <div>
                <span>Startdatum</span>
                <DatePicker
                  selected={startDate}
                  onChange={(date) => setStartDate(date)}
                  selectsStart
                  startDate={startDate}
                  endDate={endDate}
                  dateFormat="MM/yyyy"
                  showMonthYearPicker
                />
              </div>
              <div>
                <span>Enddatum</span>
                <DatePicker
                  selected={endDate}
                  onChange={(date) => setEndDate(date)}
                  selectsEnd
                  startDate={startDate}
                  endDate={endDate}
                  dateFormat="MM/yyyy"
                  showMonthYearPicker
                />
              </div>
            </div>
          </div>
          <button className={styles.commitSearchButton} onClick={commitSearch}>
            Suche abspeichern
          </button>
        </div>
      </div>
      <div className={styles.main}>
        <div className={styles.search}>
          <div className={styles.searchInputContainer}>
            <SearchInput {...searchInputProps} />
          </div>
          <div className={styles.searchControls}>
            <Select {...sortBySelectProps} />
            <div className={styles.switchRow}>
              <label htmlFor="tags-toggle">Tags anzeigen</label>
              <Switch id="tags-toggle" {...toggleTagsProps} />
            </div>
          </div>
        </div>
        <div className={styles.items}>
          {mediaItems.map((item) => (
            <ResultAdapter
              key={item.id}
              showTags={toggleTagsProps.checked}
              {...item}
            />
          ))}
        </div>
        {lastMediaItem && hasMoreItems && (
          <LoadMoreButton onClick={requestMoreItems} />
        )}
      </div>
    </div>
  );
};

export default Filter;
