import React, { memo, useCallback } from 'react';
import PropTypes from 'prop-types';
import { useToggle } from 'react-use';
import { capitalize, defaultTo, first, last } from 'lodash';
import ResultBar from '../../../components/result-bar';
import ResultDetail from '../../../components/result-detail';
import TagBar from '../../../components/tag-bar';

/**
 * Joins a range of classes to a single string. If classes has just a single
 * element than simply that will be returned.
 *
 * @author Ruben Smidt
 * @param {array} classes
 * @returns {string} a string based on the classes
 */
const joinClasses = (classes) => {
  const fromClass = first(classes);
  const toClass = last(classes);
  return fromClass === toClass
    ? fromClass.toString()
    : `${fromClass}-${toClass}`;
};

/**
 * An adapter that maps data to the format required by the generic result
 * component.
 *
 * @author Ruben Smidt
 */
const ResultAdapter = ({
  id,
  title,
  rating,
  downloads,
  owner,
  materialType,
  format,
  subjects,
  dateCreated,
  dateUpdated,
  tags,
  gradeLevels,
  showTags = true,
}) => {
  const [isExpanded, toggleIsExpanded] = useToggle(false);
  const expandResult = useCallback(() => {
    toggleIsExpanded();
  }, [toggleIsExpanded]);

  const subject = defaultTo(capitalize(first(subjects)), 'Kein Fach');
  const createdAt = new Date(dateCreated).toLocaleDateString();
  const updatedAt = new Date(dateUpdated).toLocaleDateString();

  return (
    <div>
      {isExpanded ? (
        <ResultDetail
          description={title}
          author={owner.username}
          dataFormat={format}
          uploadDate={createdAt}
          changeDate={updatedAt}
          downloads={downloads}
          href={`https://backend.bm4oer.de/media/${id}`}
          evaluation={rating.toString()}
          gradeLevel={joinClasses(gradeLevels)}
          tags={tags}
          media={capitalize(materialType)}
          subject={subject}
          onClick={expandResult}
        />
      ) : (
        <ResultBar
          author={owner.username}
          date={createdAt}
          format={format}
          subject={subject}
          evaluation={rating.toString()}
          description={title}
          dType={capitalize(materialType)}
          onClick={expandResult}
        />
      )}

      {showTags && !isExpanded && <TagBar tags={tags} />}
    </div>
  );
};

ResultAdapter.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  rating: PropTypes.number.isRequired,
  downloads: PropTypes.number.isRequired,
  owner: PropTypes.shape({
    username: PropTypes.string.isRequired,
    picture: PropTypes.string,
  }).isRequired,
  materialType: PropTypes.string.isRequired,
  format: PropTypes.string.isRequired,
  subjects: PropTypes.arrayOf(PropTypes.string).isRequired,
  dateCreated: PropTypes.string.isRequired,
  dateUpdated: PropTypes.string.isRequired,
  tags: PropTypes.arrayOf(PropTypes.string).isRequired,
  gradeLevels: PropTypes.arrayOf(PropTypes.number),
  showTags: PropTypes.bool,
};

export default memo(ResultAdapter);
