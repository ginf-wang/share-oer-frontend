import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import styles from './search-input.module.scss';

/**
 * The default search input filter component.
 *
 * Proxy: input
 *
 * @author Ruben Smidt
 */
const SearchInput = ({ onCommit, ...rest }) => {
  return (
    <div className={styles.searchInput}>
      <FontAwesomeIcon
        className={styles.icon}
        onClick={onCommit}
        icon="search"
        size="lg"
      />
      <input {...rest} />
    </div>
  );
};

SearchInput.propTypes = {
  onCommit: PropTypes.func.isRequired,
};

export default SearchInput;
