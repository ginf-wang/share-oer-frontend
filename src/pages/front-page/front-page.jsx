import React, { useCallback } from 'react';
import HomePage from '../../components/home-page';
import LoginWindow from '../../components/login-window';
import frontPageStyles from './front-page.module.scss';
import { useAuth } from '../../hooks/auth';

/**
 * The front page offers interfaces to login.
 *
 * @author Ruben Smidt
 */
const FrontPage = () => {
  const { isAuthenticated, loginUser } = useAuth();

  const login = useCallback(
    ({ email, password }) => {
      loginUser({ email, password });
    },
    [loginUser]
  );

  return (
    <>
      <div className={frontPageStyles.outlet}>
        <HomePage />
        {!isAuthenticated && <LoginWindow onSubmit={login} />}
      </div>
    </>
  );
};

export default FrontPage;
