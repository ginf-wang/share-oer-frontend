import { first } from 'lodash';
import { atom } from 'recoil';

// Holds all messages of a user.
export const userMessagesState = atom({
  key: 'BM4OER/community/messages',
  default: [],
});

export const userDialogsState = atom({
  key: 'BM4OER/community/dialogs',
  default: null,
});

export const selectReceiverFromDialog = (
  dialog,
  user = { username: 'Mervi' }
) => {
  let receiver;
  const messageFromMe = dialog.find(
    (msg) => msg.sender.username === user.username
  );
  if (!messageFromMe) {
    receiver = first(dialog).sender.username;
  } else {
    receiver = messageFromMe.receiver.username;
  }
  return receiver;
};
