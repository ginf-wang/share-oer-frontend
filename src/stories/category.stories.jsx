import React from 'react';
import Category from '../components/category';

export default {
  title: 'Category',
  component: Category,
};

const exampleData = [
  {
    title: 'Klassenstufe',
    children: [
      '5.Klasse',
      '6.Klasse',
      '7.Klasse',
      '8.Klasse',
      '9.Klasse',
      '10.Klasse',
      '11.Klasse',
      '12.Klasse',
      '13.Klasse',
    ],
  },
  {
    title: 'Fächer',
    children: [
      'Deutsch',
      'Mathe',
      'Englisch',
      'Biologie',
      'Chemie',
      'Informatik',
      'BWL',
      'Kunst',
      'Geschichte',
      'Religion',
      'Ethik',
      'Sport',
      'Sonstige',
    ],
  },
  {
    title: 'Lerntexte',
    children: [
      'Zusammenfassungen',
      'Arbeitsblätter',
      'Vorlesungen',
      'Videos',
      'Klausuren',
      'Sprachaufzeichnungen',
      'Bildergeschichten',
      'Sonstige',
    ],
  },
];

export const Klassenstufen = () => {
  return (
    <Category headline={exampleData[0].title} items={exampleData[0].children} />
  );
};

export const Faecher = () => {
  return (
    <Category headline={exampleData[1].title} items={exampleData[1].children} />
  );
};

export const Lerntexte = () => {
  return (
    <Category headline={exampleData[2].title} items={exampleData[2].children} />
  );
};
