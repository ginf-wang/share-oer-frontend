import { action } from '@storybook/addon-actions';
import React from 'react';
import Contact from '../components/contacts';

export default {
  component: Contact,
  title: 'Contacts',
};

const defaultData = [
  {
    id: '1',
    name: 'Sarah Müller',
    img:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/PICA.jpg/1200px-PICA.jpg',
  },
  {
    id: '2',
    name: 'Karola Ritter',
    img:
      'https://upload.wikimedia.org/wikipedia/commons/e/ef/Profilbild_19_12_2015.jpg',
  },
  {
    id: '3',
    name: 'Dr. Johannes Melcher der Zweite',
    img:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/PICA.jpg/1200px-PICA.jpg',
  },
  {
    id: '4',
    name: 'Dr. Sonja Ernst',
    img:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/PICA.jpg/1200px-PICA.jpg',
  },
  {
    id: '5',
    name: 'Dr. Helmut Kran',
    img:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/PICA.jpg/1200px-PICA.jpg',
  },
  {
    id: '6',
    name: 'test6',
    img:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/PICA.jpg/1200px-PICA.jpg',
  },
  {
    id: '7',
    name: 'test7',
    img:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/PICA.jpg/1200px-PICA.jpg',
  },
  {
    id: '8',
    name: 'test8',
    img:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/PICA.jpg/1200px-PICA.jpg',
  },
  {
    id: '9',
    name: 'test9',
    img:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/PICA.jpg/1200px-PICA.jpg',
  },
];

export const Contacts = () => (
  <Contact
    users={defaultData}
    headline={'Kontakte'}
    icon={'user-plus'}
    onAddContact={action('on add contact')}
  />
);
