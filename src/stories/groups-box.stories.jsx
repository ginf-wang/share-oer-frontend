import { action } from '@storybook/addon-actions';
import React from 'react';
import Groups from '../components/groups-box';

export default {
  component: Groups,
  title: 'Groups',
};

const defaultData = [
  {
    id: '1',
    name: '5a Hermann Gesamtschule',
    img:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/PICA.jpg/1200px-PICA.jpg',
    lastMessage: 'Ich: hallo super funktioniert das',
  },
  {
    id: '2',
    name: '7b Hermann Gesamtschule',
    img:
      'https://upload.wikimedia.org/wikipedia/commons/e/ef/Profilbild_19_12_2015.jpg',
    lastMessage: 'Ich: hallo super funktioniert das',
  },
  {
    id: '3',
    name: 'Abi Vorbereitungskurs',
    img:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/PICA.jpg/1200px-PICA.jpg',
    lastMessage: 'Ich: hallo super funktioniert das',
  },
  {
    id: '4',
    name: 'Abi Vorbereitungskurs',
    img:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/PICA.jpg/1200px-PICA.jpg',
    lastMessage: 'Ich: hallo super funktioniert das',
  },
  {
    id: '5',
    name: 'Abi Vorbereitungskurs',
    img:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/PICA.jpg/1200px-PICA.jpg',
    lastMessage: 'Ich: hallo super funktioniert das',
  },
  {
    id: '6',
    name: 'Abi Vorbereitungskurs',
    img:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/PICA.jpg/1200px-PICA.jpg',
    lastMessage: 'Ich: hallo super funktioniert das',
  },
];

export const Group = () => (
  <Groups
    groups={defaultData}
    headline={'Meine Gruppen'}
    icon={'user-plus'}
    onAddMessage={action('on new Group')}
    onMembers={action('on Members')}
    onSettings={action('on Settings')}
    onShare={action('on Share')}
  />
);
