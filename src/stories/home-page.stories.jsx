import React from 'react';
import HomePage from '../components/home-page';

export default {
  title: 'Homepage',
  component: HomePage,
};

export const Default = () => <HomePage />;
