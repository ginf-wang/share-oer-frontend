import { action } from '@storybook/addon-actions';
import React from 'react';
import LoadMoreButton from '../components/load-more-button';

export default {
  title: 'Load-More-Button',
  component: LoadMoreButton,
};

export const Default = () => <LoadMoreButton onClick={action('click')} />;
