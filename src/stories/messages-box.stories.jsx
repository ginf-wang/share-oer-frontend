import { action } from '@storybook/addon-actions';
import React from 'react';
import Messages from '../components/messages-box';

export default {
  component: Messages,
  title: 'Messages',
};

const defaultData = [
  {
    id: '1',
    name: 'Sarah Müller',
    img:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/PICA.jpg/1200px-PICA.jpg',
    lastMessage: 'Ich: hallo super funktioniert das',
  },
  {
    id: '2',
    name: 'Karola Ritter',
    img:
      'https://upload.wikimedia.org/wikipedia/commons/e/ef/Profilbild_19_12_2015.jpg',
    lastMessage: 'Ich: hallo super funktioniert das',
  },
  {
    id: '3',
    name: 'Dr. Johannes Melcher der Zweite',
    img:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/PICA.jpg/1200px-PICA.jpg',
    lastMessage: 'Ich: hallo super funktioniert das',
  },
  {
    id: '4',
    name: 'Dr. Sonja Ernst',
    img:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/PICA.jpg/1200px-PICA.jpg',
    lastMessage: 'Ich: hallo super funktioniert das',
  },
  {
    id: '5',
    name: 'Dr. Helmut Kran',
    img:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/PICA.jpg/1200px-PICA.jpg',
    lastMessage: 'Ich: hallo super funktioniert das',
  },
  {
    id: '6',
    name: 'test6',
    img:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/PICA.jpg/1200px-PICA.jpg',
    lastMessage: 'Ich: hallo super funktioniert das',
  },
  {
    id: '7',
    name: 'test7',
    img:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/PICA.jpg/1200px-PICA.jpg',
    lastMessage: 'Ich: hallo super funktioniert das',
  },
  {
    id: '8',
    name: 'test8',
    img:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/PICA.jpg/1200px-PICA.jpg',
    lastMessage: 'Ich: hallo super funktioniert das',
  },
  {
    id: '9',
    name: 'test9',
    img:
      'https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/PICA.jpg/1200px-PICA.jpg',
    lastMessage: 'Ich: hallo super funktioniert das',
  },
];

export const Message = () => (
  <Messages
    users={defaultData}
    headline={'Privatnachrichten'}
    icon={'user-plus'}
    onAddMessage={action('on new Message')}
    onShareMessage={action('on Share')}
    onSettingsMessage={action('on Settings')}
  />
);
