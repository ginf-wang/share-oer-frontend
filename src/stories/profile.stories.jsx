import React from 'react';
import Profile from '../components/profile';

export default {
  component: Profile,
  title: 'Profile',
};

const defaultData = {
  pic:
    'https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/PICA.jpg/1200px-PICA.jpg',
  name: 'Felix Krimps',
  memberSince: '15.03.2020',
};

export const Default = () => {
  return (
    <Profile
      picture={defaultData.pic}
      name={defaultData.name}
      memberSince={defaultData.memberSince}
    />
  );
};
