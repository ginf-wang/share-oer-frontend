import React, { useState } from 'react';
import RadioFilter from '../components/radio-filter';

const exampleOptions = ['Aufgaben', 'Klausuren', 'Loesungen'];

export default {
  title: 'Radio Filter',
  component: RadioFilter,
};

export const Default = () => {
  const [activeIndex, setActiveIndex] = useState(0);

  return (
    <RadioFilter
      onOptionChange={(option) =>
        setActiveIndex(exampleOptions.indexOf(option))
      }
      options={exampleOptions}
      activeOption={exampleOptions[activeIndex]}
    />
  );
};
