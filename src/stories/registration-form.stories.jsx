import React from 'react';
import RegistrationForm from '../components/registration-form';

export default {
  title: 'Registration Form',
  component: RegistrationForm,
};

export const Test = () => <RegistrationForm />;
