import React from 'react';
import ResultBar from '../components/result-bar';

export default {
  title: 'Result Bar',
  component: ResultBar,
};

export const Test = () => (
  <ResultBar
    description={'Kreis: Fläche und Umfang berechnen'}
    format={'Aufgaben'}
    dType={'.pdf'}
    subject={'Mathematik'}
    author={'Peter Peters'}
    date={'01.08.2019'}
    evaluation={'3,5 / 5'}
  />
);
