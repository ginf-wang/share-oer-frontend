import React from 'react';
import ResultDetailInfo from '../components/result-detail/result-detail-info';
import ResultDetail from '../components/result-detail/result-detail';

export default {
  component: ResultDetailInfo,
  title: 'Result Detail',
};

export const Default = () => (
  <ResultDetail
    author={'Max mustermann'}
    media={'Aufgaben'}
    changeDate={'1.1.20'}
    dataFormat={'.pdf'}
    downloads={2000}
    evaluation={'5'}
    size={10 + 'kb'}
    subject={'Deutsch'}
    comments={200}
    gradeLevel={'5'}
    uploadDate={'1.1.19'}
    description={'Kreis: Fläche und Umfang berechnen'}
    tags={['Geometrie', 'Biologie', 'Mathematik', 'Deutsch', 'Chemie', 'Kunst']}
    href="google.com"
  />
);

export const Detail = () => (
  <ResultDetailInfo
    author={'Max mustermann'}
    media={'Aufgaben'}
    changeDate={'1.1.20'}
    dataFormat={'.pdf'}
    downloads={2000}
    evaluation={'5'}
    size={10 + 'kb'}
    subject={'Deutsch'}
    comments={200}
    gradeLevel={'5'}
    uploadDate={'1.1.19'}
    description={'Kreis: Fläche und Umfang berechnen'}
  />
);
