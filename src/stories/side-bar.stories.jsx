import React from 'react';
import SideBar from '../components/side-bar';

export default {
  component: SideBar,
  title: 'Side bar',
};

export const Default = () => <SideBar />;
