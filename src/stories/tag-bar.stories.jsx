import React from 'react';
import TagBar from '../components/tag-bar/tag-bar';

export default {
  title: 'Tag Bar',
  component: TagBar,
};

export const WithThreeItems = () => (
  <TagBar tags={['Geometrie', 'Mathematik', 'Biologie']} />
);

export const WithOneItems = () => <TagBar tags={['Geometrie']} />;

export const WithSixItems = () => (
  <TagBar
    tags={['Geometrie', 'Biologie', 'Mathematik', 'Deutsch', 'Chemie', 'Kunst']}
  />
);
